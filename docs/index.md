# valid8-schema

For documentation see tests: [https://gitlab.com/python-toolbelt/valid8-schema/-/tree/main/tests](https://gitlab.com/python-toolbelt/valid8-schema/-/tree/main/tests)

## Want to contribute?

Contributions are welcome! Simply fork this project on gitlab, commit your contributions, and create merge requests.

Here is a non-exhaustive list of interesting open topics: [https://gitlab.com/python-toolbelt/valid8-schema/-/issues](https://gitlab.com/python-toolbelt/valid8-schema/-/issues)

## Requirements

Install requirements for setup beforehand using

```bash
poetry install -E test
```

## Running the tests

This project uses `pytest`.

```bash
pytest
```

## License

This project is licensed under the terms of the MIT license. See [LICENSE](https://gitlab.com/python-toolbelt/valid8-schema/-/blob/main/LICENSE) for more information.
