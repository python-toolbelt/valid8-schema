# Changelog

Tracking changes in `valid8-schema` between versions. For a complete view of all the releases, visit GitLab:

https://gitlab.com/python-toolbelt/valid8-schema/-/releases

## next release
